package com.bsa.giphy.bsa_giphy.dto;

import org.springframework.stereotype.Component;

@Component
public class Cache {
    private String query;
    private String[] gifs;

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public String[] getGifs() {
        return gifs;
    }

    public void setGifs(String[] gifs) {
        this.gifs = gifs;
    }
}
