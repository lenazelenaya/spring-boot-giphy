package com.bsa.giphy.bsa_giphy.entities;

import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class Repo {
    private Map<String, Map<String, List<String>>> map;

    private Repo(){
        map = new HashMap<>();
    }

    public void updateCache(String user_id, String query, String gifName){
        List<String> list = new ArrayList<>();
        if(this.map.get(user_id) != null){
            if(this.map.get(user_id).get(query) != null){
                list = this.map.get(user_id).get(query);
                list.add(list.size(), gifName);
                this.map.get(user_id).put(query, list);
            } else {
                list.add(gifName);
                this.map.get(user_id).put(query, list);
            }
        } else {
            Map<String, List<String>> userMap = new HashMap<>();
            list.add(gifName);
            userMap.put(query, list);
            this.map.put(user_id, userMap);
        }
    }

    public String getGifFromCache(String user_id, String query) {
        if(this.map.get(user_id) != null) {
            if(this.map.get(user_id).get(query) != null) {
                List<String> queryList = this.map.get(user_id).get(query);
                return queryList.get(new Random().nextInt(queryList.size()));
            }
        }
        return null;
    }

    public void resetUser(String user_id) {
        this.map.remove(user_id);
    }

    public void resetUser(String user_id, String query){
        this.map.get(user_id).remove(query);
    }
}
