package com.bsa.giphy.bsa_giphy.entities;

public class Gif {

    private String pathInFS;
    private String name;
    private String query;

    public String getPathInFS() {
        return pathInFS;
    }

    public void setPathInFS(String pathInFS) {
        this.pathInFS = pathInFS;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }
}
