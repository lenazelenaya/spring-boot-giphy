package com.bsa.giphy.bsa_giphy.exceptions;

public class NoHeaderException extends RuntimeException {
    public NoHeaderException () {
        super();
    }

    public NoHeaderException (String message) {
        super(message);
    }
}
