package com.bsa.giphy.bsa_giphy.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.Map;

@ControllerAdvice
public class Handler extends ResponseEntityExceptionHandler {
    @ExceptionHandler(RuntimeException.class)
    public ResponseEntity<?> handleNoBsaGiphyException(NoHeaderException ex) {
        return new ResponseEntity<>(Map.of("message", "Request not assembled necessary headers!"),
                HttpStatus.FORBIDDEN);
    }
}
