package com.bsa.giphy.bsa_giphy.services;

import com.bsa.giphy.bsa_giphy.dto.GifDto;
import com.bsa.giphy.bsa_giphy.entities.Gif;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

@Service
public class GiphyService {

    Environment environment;

    @Autowired
    public GiphyService(Environment environment){
        this.environment = environment;
    }

    public Gif searchGif(String user_id, String query){
        RestTemplate restTemplate = new RestTemplate();
        UriComponentsBuilder builder = UriComponentsBuilder
                .fromHttpUrl(environment.getProperty("giphy_url"))
                .queryParam("api_key", environment.getProperty("api_key"))
                .queryParam("tag", query)
                .queryParam("random_id", user_id);

        GifDto gifFile = restTemplate.getForObject(builder.toUriString(), GifDto.class);

        JSONObject json = new JSONObject(gifFile);
        json = json.getJSONObject("data");

        Gif gif = new Gif();
        gif.setName(json.getString("id"));

        StringBuilder url = new StringBuilder(json.getJSONObject("images")
                .getJSONObject("downsized")
                .getString("url"));

        url.replace(8, 14, "i");

        gif.setPathInFS(url.toString());
        gif.setQuery(query);

        return gif;
    }
}
