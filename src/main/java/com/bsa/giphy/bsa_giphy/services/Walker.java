package com.bsa.giphy.bsa_giphy.services;

import com.bsa.giphy.bsa_giphy.dto.Cache;
import com.bsa.giphy.bsa_giphy.dto.LoggerFile;
import org.springframework.stereotype.Service;

import java.io.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class Walker {
    public Cache[] walkAllCache(Map<String, File[]> withQueryMap) {
        Cache[] cache = new Cache[withQueryMap.size()];
        int counter = 0;
        int cacheCounter = 0;
        for (Map.Entry<String, File[]> gif : withQueryMap.entrySet()) {
            String[] array = new String[gif.getValue().length];
            for (File file : withQueryMap.get(gif.getKey())) {
                array[counter] = file.getAbsolutePath();
                counter++;
            }
            cache[cacheCounter] = new Cache();
            cache[cacheCounter].setQuery(gif.getKey());
            cache[cacheCounter].setGifs(array);
            counter = 0;
            cacheCounter++;
        }
        return cache;
    }

    public String[] getOnlyFiles(Map<String, File[]> queriedCache) {
        Cache[] cacheDto = walkAllCache(queriedCache);
        return cacheDto[0].getGifs();
    }

    public List<LoggerFile> logWalker(File file) {
        List<LoggerFile> historyList = new ArrayList<>();
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file)))) {
            String line;
            while ((line = reader.readLine()) != null) {
                LoggerFile history = new LoggerFile();
                String[] result = line.split(",");
                history.setDate(LocalDate.parse(result[0]));
                history.setQuery(result[1]);
                history.setGif(result[2]);
                historyList.add(history);
            }
        } catch (IOException ex) {
            System.out.println("Impossible to read file");
        }
        return historyList;
    }

    public LoggerFile[] getLog(File file) {
        var resultList = logWalker(file);
        var resultArray = new LoggerFile[resultList.size()];
        return resultList.toArray(resultArray);
    }
}
