package com.bsa.giphy.bsa_giphy.services;

import com.bsa.giphy.bsa_giphy.entities.Gif;
import org.apache.commons.io.FileUtils;
import org.springframework.stereotype.Service;

import java.io.*;
import java.net.URL;
import java.nio.file.Files;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

@Service
public class FsService {
    private final String root = "c:\\bsa_giphy\\";
    private final String cache = root + "cache\\";
    private final String users = root + "users\\";

    public File downloadGifFromFS(Gif gifName) {
        try ( BufferedInputStream inputStream = new BufferedInputStream(
                new URL(gifName.getPathInFS()).openStream())) {
            File gifInFSCache = new File(cache + gifName.getQuery());
            if(!gifInFSCache.exists()) {
                gifInFSCache.mkdir();
            }
            File gif = new File(gifInFSCache, gifName.getName() + ".gif");

            try(FileOutputStream fileOutputStream = new FileOutputStream(gif)) {
                byte[] buffer = new byte[1000];
                int inputBytes;

                while ((inputBytes = inputStream.read(buffer, 0, 1000)) != -1) {
                    fileOutputStream.write(buffer, 0, inputBytes);
                }
            }catch (IOException ex){
                ex.printStackTrace();
            }
            return gif;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public File getGifPath(Gif gif){
        File cacheGif = new File(cache + gif.getQuery(), gif.getName() + ".gif");
        return cacheGif.exists() ? cacheGif : null;
    }

    public File getGifPath(String query) {
        File gif = new File(cache + query);
        File[] files = gif.listFiles();
        return files != null ? files[new Random().nextInt(files.length)] : null;
    }

    public void addGifToUser(String user_id, Gif gif) {
        String pathToGif = users +  user_id + "\\" + gif.getQuery() + "\\";
        File directory = new File(pathToGif);

        if(!directory.exists()) {
            directory.mkdirs();
        }

        File cacheGif = getGifPath(gif);

        if(cacheGif != null) cacheGif = new File(cacheGif.getAbsolutePath());
        else cacheGif = downloadGifFromFS(gif);

        copyToUserFromCache(user_id, gif.getQuery(), cacheGif.getPath());
    }
    
    public String getGifFromUser(String user_id, String query){
        File userGifDir = new File(users + user_id, query);
        if(userGifDir.listFiles() != null)
            return userGifDir.listFiles()[new Random().nextInt(userGifDir.listFiles().length)].toString();
        else return null;
    }

    public File copyToUserFromCache(String user_id, String query, String cacheGif) {
        File source = new File(cacheGif);
        File copiedTo = new File(users + user_id + "\\" + query);
        try {
            copiedTo.mkdirs();
            copiedTo = new File(copiedTo, source.getName());
            Files.copy(source.toPath(), copiedTo.toPath());
            //logger(user_id, query, copiedTo);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return new File(copiedTo, source.getName());
    }
/*
    public void logger(String user_id, String query, File savedFile) {
        File userFile = new File(users + user_id, "history.csv");
        try (PrintStream printer = new PrintStream(new FileOutputStream(userFile, true))) {

            if(!userFile.exists()) {
                userFile.createNewFile();
            }

            String history = LocalDate.now().toString() + "," + query + "," + savedFile.getAbsolutePath();

            printer.println(history);

        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
    */

    public void clearFSCache() {
        File cache = new File(this.cache);
        cache.delete();
    }

    public void clearUserDir(String user_id) {
        File userCache = new File(users + user_id);
        try { FileUtils.forceDelete(userCache); }
        catch (IOException ex) {
            System.out.println(ex);
        }
    }

    public Map<String, File[]> getAllFSCache(String query){
        return getAllFiles(query, cache);
    }

    public Map<String, File[]> getAllUserFiles(String user_id){
        return getAllFiles(null, users + user_id);
    }

    public Map<String, File[]> getAllFiles(String query, String path) {
        if(query != null){
            File[] files = getCacheWithQuery(query);
            Map map = new HashMap<String, File[]>();
            map.put(query, files);
            return map;
        } else {
            int counter = 0;
            File genFile = new File(path);
            var map = new HashMap<String, File[]>();
            File[] childs = new File[genFile.listFiles().length];
            for(File gif : genFile.listFiles()) {
                childs[counter] = gif;
                counter++;
            }

            map.put("gifs", childs);
            return map;
        }
    }

    public File[] getCacheWithQuery(String query) {
        var file = new File(cache + query);
        File[] files = file.listFiles();
        return files;
    }

    public File getLogFile(String user_id) {
        return new File(users + user_id + "\\history.csv");
    }

    public boolean deleteLogFile(String user_id) {
        return new File(users + user_id + "\\history.csv").delete();
    }


}
