package com.bsa.giphy.bsa_giphy.controllers;

import com.bsa.giphy.bsa_giphy.dto.LoggerFile;
import com.bsa.giphy.bsa_giphy.entities.Gif;
import com.bsa.giphy.bsa_giphy.entities.Repo;
import com.bsa.giphy.bsa_giphy.services.FsService;
import com.bsa.giphy.bsa_giphy.services.GiphyService;
import com.bsa.giphy.bsa_giphy.services.Walker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotBlank;
import java.io.File;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/user/")
public class Controller {
    private final GiphyService giphyService;
    private final FsService fsService;
    private final Repo repo;
    private final Walker walker;

    Logger logger = LoggerFactory.getLogger(Controller.class);

    @Autowired
    public Controller(GiphyService giphyService, FsService fsService, Repo repo, Walker walker){
        this.giphyService = giphyService;
        this.fsService = fsService;
        this.repo = repo;
        this.walker = walker;
    }

    @PostMapping("{user_id}")
    public ResponseEntity<?> generateGif(@RequestParam @PathVariable String user_id,
                                         @NotBlank @RequestBody String query,
                                         @RequestHeader(name="X-BSA-GIPHY") boolean present) {
        if(invalidString(user_id) != null)
            return invalidString(user_id);

        System.out.println(present);

        Map<String, String> response = new HashMap<>();

        logger.info("Post request by: " + user_id + "Method: generateGif(...); QUERY: " + query);

        File gifFile = fsService.getGifPath(query);
        if (gifFile != null) {
            File result = fsService.copyToUserFromCache(user_id, query, gifFile.getPath());
            response.put("gif", result.getAbsolutePath());
            response.put("query", query);

            repo.updateCache(user_id, query, result.getName());
        } else {
            Gif gif = giphyService.searchGif(user_id, query);
            fsService.addGifToUser(user_id, gif);
            gifFile = fsService.getGifPath(query);
            response.put("gif", gifFile.getAbsolutePath());
            response.put("query", query);

            repo.updateCache(user_id, query, gif.getName());
        }

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("{user_id}")
    public ResponseEntity<?> searchGif(@PathVariable String user_id, @NotBlank @RequestBody String query) {
        if(invalidString(user_id) != null)
            return invalidString(user_id);
        if(invalidString(query) != null)
            return invalidString(query);
        logger.info("Get request by: " + user_id + ", Method: searchGif(), QUERY: " + query);

        Map<String, String> response = new HashMap<>();

        String gifFromCache, gifFromUser;

        gifFromCache = repo.getGifFromCache(user_id, query);

        if(gifFromCache != null){
            response.put("gif", gifFromCache + ".gif");
            response.put("query", query);
            return new ResponseEntity<>(response, HttpStatus.OK);
        }else {
            gifFromUser = fsService.getGifFromUser(user_id, query);
            if(gifFromUser != null){
                response.put("gif", gifFromUser);
                response.put("query", query);
                return new ResponseEntity<>(response, HttpStatus.OK);
            }else {
                return new ResponseEntity<>(response, HttpStatus.NO_CONTENT);
            }
        }
    }

    @GetMapping("{user_id}/all")
    public ResponseEntity<?> getAllGifs(@PathVariable String user_id) {
        if(invalidString(user_id) != null) return invalidString(user_id);
        logger.info("Get request by: " + user_id +", Method: getAllGifs()");
        var result = walker.walkAllCache(fsService.getAllUserFiles(user_id));
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @GetMapping("{user_id}/history")
    public ResponseEntity<?> getHistory(@PathVariable String user_id) {
        if(invalidString(user_id) != null) return invalidString(user_id);
        logger.info("Get request by: " + user_id + ", Method: getHistory()");
        LoggerFile[] log = walker.getLog(fsService.getLogFile(user_id));
        if(log != null) {
            return new ResponseEntity<>(log, HttpStatus.OK);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("{user_id}/history/clean")
    public ResponseEntity<?> cleanHistory(@PathVariable String user_id) {
        if(invalidString(user_id) != null) return invalidString(user_id);
        logger.info("Delete request by: " + user_id + ", Method: cleanHistory()");
        if(fsService.deleteLogFile(user_id)) {
            return ResponseEntity.ok().build();
        } else {
            return ResponseEntity.badRequest().build();
        }
    }

    @GetMapping("{user_id}/search")
    public ResponseEntity<?> searchGif(@PathVariable String user_id, String query, boolean force) {
        if(invalidString(user_id) != null) return invalidString(user_id);
        logger.info("Get request by: " + user_id + ", Method: searchGif()");
        if(!force) {
            String gif = repo.getGifFromCache(user_id, query);
            if(gif != null) {
                return new ResponseEntity<>(gif, HttpStatus.OK);
            }
        }
        File gif = fsService.getGifPath(query);
        if(gif == null) {
            return ResponseEntity.notFound().build();
        }
        repo.updateCache(user_id, query, gif.getAbsolutePath());
        return new ResponseEntity<>(gif.getAbsolutePath(), HttpStatus.OK);
    }

    @PostMapping("{user_id}/generate")
    public ResponseEntity<?> createGif(@PathVariable String user_id, @RequestBody String query, boolean force) {
        if(invalidString(user_id) != null) return invalidString(user_id);
        logger.info("Post request by: " + user_id + ", Method: createGif(), query: " + query + ", force: " + force);
        File gifFile;
        if(!force) {
            gifFile = fsService.getGifPath(query);
            if(gifFile != null) {
                return new ResponseEntity<>(gifFile.getAbsoluteFile(), HttpStatus.OK);
            }
        }
        Gif gif = giphyService.searchGif(user_id, query);
        fsService.addGifToUser(user_id, gif);
        gifFile = fsService.getGifPath(query);

        return new ResponseEntity<>(gifFile.getAbsolutePath(), HttpStatus.OK);
    }

    @DeleteMapping("{user_id}/reset")
    public ResponseEntity<?> clearUserDir(@PathVariable String user_id, String query) {
        if(invalidString(user_id) != null) return invalidString(user_id);
        logger.info("Delete request by: " + user_id + ", Method: clearUserDir()");
        if(query == null) {
            repo.resetUser(user_id);
        } else {
            repo.resetUser(user_id, query);
        }
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("{user_id}/clean")
    public ResponseEntity<?> deleteUser(@PathVariable String user_id) {
        if(invalidString(user_id) != null) return invalidString(user_id);
        logger.info("Delete request by: " + user_id +", Method: deleteUser()");
        repo.resetUser(user_id);
        fsService.clearUserDir(user_id);
        return ResponseEntity.ok().build();
    }

    public ResponseEntity<?> invalidString(String string){
        if (string.isBlank()
                || string.isEmpty()
                || string.matches(".*[|*?<>:/\"].*"))
            return new ResponseEntity<>("Invalid request", HttpStatus.BAD_REQUEST);
            return null;
    }

}
