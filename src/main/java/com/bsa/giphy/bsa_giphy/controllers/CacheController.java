package com.bsa.giphy.bsa_giphy.controllers;

import com.bsa.giphy.bsa_giphy.dto.Cache;
import com.bsa.giphy.bsa_giphy.entities.Gif;
import com.bsa.giphy.bsa_giphy.entities.Repo;
import com.bsa.giphy.bsa_giphy.services.FsService;
import com.bsa.giphy.bsa_giphy.services.GiphyService;
import com.bsa.giphy.bsa_giphy.services.Walker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class CacheController {
    GiphyService giphyService;
    FsService fsService;
    Repo repo;
    Walker walker;

    Logger logger = LoggerFactory.getLogger(Controller.class);

    @Autowired
    public CacheController(GiphyService giphyService, FsService fsService, Repo repo, Walker walker){
        this.giphyService = giphyService;
        this.fsService = fsService;
        this.repo = repo;
        this.walker = walker;
    }
    @GetMapping("/cache")
    public ResponseEntity<?> getFSCache(String query) {
        logger.info("Get request, Method: getFSCache(...), Query: " + query);

        Cache[] fullCache = walker.walkAllCache(fsService.getAllFSCache(query));
        return new ResponseEntity<>(fullCache, HttpStatus.OK);
    }

    @DeleteMapping("/cache")
    public ResponseEntity<?> clearDiskCache() {

        logger.info("Delete request, Method: clearDiskCache()");
        fsService.clearFSCache();
        return ResponseEntity.ok().build();
    }

    @PostMapping("/cache/generate")
    public ResponseEntity<?> generateGif(@RequestBody String query) {
        logger.info("Post request, Method: generateGif(), Query: " + query);
        Gif giphy = giphyService.searchGif(null, query);
        fsService.downloadGifFromFS(giphy);
        Cache cache = walker.walkAllCache(fsService.getAllFSCache(query))[0];
        return new ResponseEntity<>(cache, HttpStatus.OK);
    }

    @GetMapping("/gifs")
    public ResponseEntity<?> getAllGifs() {
        logger.info("Get request, Method: getAllGifs()");
        var allGifs = walker.getOnlyFiles(fsService.getAllFSCache(null));
        return new ResponseEntity<>(allGifs, HttpStatus.OK);
    }

}
