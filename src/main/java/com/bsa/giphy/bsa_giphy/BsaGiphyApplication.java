package com.bsa.giphy.bsa_giphy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Map;

@SpringBootApplication
public class BsaGiphyApplication {

	public static void main(String[] args) {
		SpringApplication.run(BsaGiphyApplication.class, args);
	}

}
